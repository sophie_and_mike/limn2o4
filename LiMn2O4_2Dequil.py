from sys import argv

LiMn2O4 = argv[0]
n = int(argv[1])
x = int(argv[2])

import numpy as np
import random
import xlwt


def Ulattice(lattice, dim):  # Calculates the internal energy of lattice1
    Ulatt = 0  # Start energy at 0

    for i in range(dim):
        for j in range(dim):

                nnSUM = lattice[i, (j+1) % dim] + lattice[i, (j-1) % dim] + \
                    lattice[(i+1) % dim, j] + lattice[(i-1) % dim, j]  # nn from lattice two with PBC

                nnnSUM = 2*(lattice[(i+2) % dim, j] + lattice[(i-2) % dim, j] + \
                            lattice[i, (j+2) % dim] + lattice[i, (j-2) % dim]) + \
                            lattice[(i+1) % dim, (j+1) % dim] + lattice[(i+1) % dim, (j-1) % dim] + \
                            lattice[(i-1) % dim, (j+1) % dim] + lattice[(i-1) % dim, (j-1) % dim]
                            # nnn from same lattice as site with PBC

                Usite = 37.5*lattice[i, j]*nnSUM - 4.0*lattice[i,j]*nnnSUM - 4.12*lattice[i,j]  # Energy hamiltonian

                Ulatt += Usite # Adds site energy to the total
    Utotal = Ulatt*(1./2.)  # Factor of 1/2 for double counting
    return Utotal


def occupancy_1(lattice, dim):
    Nocc = 0
    for i in range(dim):
        if i % 2 == 0:
            for j in range(0, dim, 2):
                if lattice[i, j] == 1:  # Adds one to the count if the site is occupied
                    Nocc += 1
                else: Nocc += 0
        else:
            for j in range(1, dim, 2):
                if lattice[i, j] == 1:  # Adds one to the count if the site is occupied
                    Nocc += 1
                else: Nocc += 0

    return Nocc


def occupancy_2(lattice, dim):
    Nocc = 0
    for i in range(dim):
        if i % 2 == 1:
            for j in range(0, dim, 2):
                if lattice[i, j] == 1:
                    Nocc += 1
                else: Nocc += 0
        else:
            for j in range(1, dim, 2):
                if lattice[i, j] == 1:
                    Nocc += 1
                else: Nocc += 0
    return Nocc


def Hamiltonian(lattice, dim, a, b, mu):

    site = lattice[a, b]

    nnSUM = lattice[(a+1) % dim, b] + lattice[(a-1) % dim, b] + lattice[a, (b+1) % dim] + lattice[a, (b-1) % dim]

    nnnSUM = 2*(lattice[(a+2) % dim, b] + lattice[(a-2) % dim, b] + lattice[a, (b+2) % dim]\
                + lattice[a, (b-2) % dim]) + lattice[(a+1) % dim, (b+1) % dim] + lattice[(a+1) % dim, (b-1) % dim]\
                + lattice[(a-1) % dim, (b+1) % dim] + lattice[(a-1) % dim, (b-1) % dim]

    H1 = 37.5*site*nnSUM - 4.0*site*nnnSUM - (4.12 + mu)*site
    if lattice[a, b] == 1:
        site = 0
        H2 = 37.5*site*nnSUM - 4.0*site*nnnSUM - (4.12 + mu)*site
    else:
        site = 1
        H2 = 37.5*site*nnSUM - 4.0*site*nnnSUM - (4.12 + mu)*site

    deltaH = H2 - H1
    return deltaH


def LiMn2O4(n, x):
    """Runs a simulation of lithium intercalation into a LiMn2O4, producing 2 sublattices of the crystal structure
    :param n: number of iterations
    :param x: no. of cols and rows in arrays
    :param q: no. of equilibrium steps
    :return: two sublattices and values for...
    """
    wb = xlwt.Workbook()  # Setting up spreadsheet to write data too
    ws = wb.add_sheet("LiMn2O4", cell_overwrite_ok=True)
    ws.write(0, 0, 'Iteration')
    ws.write(0, 1, 'Sublattice 1 Occupancy')
    ws.write(0, 2, 'Sublattice 2 Occupancy')
    ws.write(0, 3, 'Lattice energy')
    row = 2

    mu = 0

    lattice = np.zeros((x, x), dtype=np.int)  # Initially empty lattice

    for itt in range(0, n):
        a = random.randint(0, (x-1))  # Generates random numbers for selection of random sites
        b = random.randint(0, (x-1))

        site = lattice[a, b]  # Selects random spin site
        deltaH = Hamiltonian(lattice, x, a, b, mu)

        if deltaH <= 0:
            if lattice[a, b] == 1:  # Change is accepted due to favourable energy change
                lattice[a, b] = 0
            else:
                lattice[a, b] = 1

        else:
            d = random.random()  # Random number generated for comparison between 0 and 1
            p = np.exp(-deltaH/298.*1.38E-23)  # Probability of spin changing

            if d > p:  # Comparison of random number and probability
                lattice[a, b] = site  # State change rejected as probability too low

            else:
                if lattice[a, b] == 1:  # Change accepted as probability is high
                    lattice[a, b] = 0
                else:
                    lattice[a, b] = 1

        if itt % 1000 == 0:
            occ1 = occupancy_1(lattice, x)
            ws.write(row, 1, occ1)

            occ2 = occupancy_2(lattice, x)
            ws.write(row, 2, occ2)

            u = Ulattice(lattice, x)
            ws.write(row, 3, u)

            ws.write(row, 0, itt)
            row += 1
            print itt

    print "lattice"
    print lattice

    wb.save('LiMn2O4_2Dequil.xls')

if __name__ == '__main__':
    LiMn2O4(n, x)

