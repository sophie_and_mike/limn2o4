from sys import argv

LiMn2O4 = argv[0]
n = int(argv[1])
x = int(argv[2])

import numpy as np
import random
import xlwt


def Ulattice(lattice1, lattice2, dim):  # Calculates the internal energy of lattice1
    Ulatt = 0  # Start energy at 0

    for i in range(dim):
        for j in range(dim):
            for k in range(dim):
                site = lattice1[i, j, k]  # Runs over every site through the i, j, k values

                nnSUM = lattice2[i, j, (k+1) % dim] + lattice2[i, j, (k-1) % dim] + \
                    lattice2[i, (j+1) % dim, k] + lattice2[i, (j-1) % dim, k]  # nn from lattice two with PBC

                nnnSUM = lattice1[i, (j+1) % dim, (k+1) % dim] + lattice1[i, (j+1) % dim, (k-1) % dim] + \
                    lattice1[i, (j-1) % dim, (k+1) % dim] + lattice1[i, (j-1) % dim, (k-1) % dim] + \
                    lattice1[(i+1) % dim, (j+1) % dim, k] + lattice1[(i+1) % dim, (j-1) % dim, k] + \
                    lattice1[(i-1) % dim, (j+1) % dim, k] + lattice1[(i-1) % dim, (j-1) % dim, k] + \
                    lattice1[(i+1) % dim, j, (k+1) % dim] + lattice1[(i+1) % dim, j, (k-1) % dim] + \
                    lattice1[(i-1) % dim, j, (k+1) % dim] + lattice1[(i-1) % dim, j, (k-1) % dim]
                    #  nnn from same lattice as site with PBC

                Usite = 37.5*site*nnSUM - 4.0*site*nnnSUM - 4.12*site  # Energy hamiltonian

                Ulatt += Usite # Adds site energy to the total
    Utotal = Ulatt*(1./4.)  # Factor of 1/2 for double counting
    return Utotal


def occupancy(lattice, dim):
    Nocc = 0
    for i in range(dim):
        for j in range(dim):
            for k in range(dim):  # Iterates over all sites
                if lattice[i, j, k] == 1:  # Adds one to the count if the site is occupied
                    Nocc += 1
                else: Nocc += 0
    return float(Nocc)


def Hamiltonian(lattice1, lattice2, dim, a, b, c, mu):

    site = lattice1[a, b, c]

    nnSUM = lattice2[(a+1) % dim, b, c] + lattice2[(a-1) % dim, b, c] + lattice2[a, (b+1) % dim, c] + \
        lattice2[a, (b-1) % dim, c]

    nnnSUM = lattice1[a, (b+1) % dim, (c+1) % dim] + lattice1[a, (b+1) % dim, (c-1) % dim] + \
        lattice1[a, (b-1) % dim, (c+1) % dim] + lattice1[a, (b-1) % dim, (c-1) % dim] + \
        lattice1[(a+1) % dim, (b+1) % dim, c] + lattice1[(a+1) % dim, (b-1) % dim, c] + \
        lattice1[(a-1) % dim, (b+1) % dim, c] + lattice1[(a-1) % dim, (b-1) % dim, c] + \
        lattice1[(a+1) % dim, b, (c+1) % dim] + lattice1[(a+1) % dim, b, (c-1) % dim] + \
        lattice1[(a-1) % dim, b, (c+1) % dim] + lattice1[(a-1) % dim, b, (c-1) % dim]

    H1 = 37.5*site*nnSUM - 4.0*site*nnnSUM - (4.12 + mu)*site
    if lattice1[a, b, c] == 1:
        site = 0
        H2 = 37.5*site*nnSUM - 4.0*site*nnnSUM - (4.12 + mu)*site
    else:
        site = 1
        H2 = 37.5*site*nnSUM - 4.0*site*nnnSUM - (4.12 + mu)*site

    deltaH = H2 - H1
    return deltaH


def LiMn2O4(n, x):
    """Runs a simulation of lithium intercalation into a LiMn2O4, producing 2 sublattices of the crystal structure
    :param n: number of iterations
    :param x: no. of cols and rows in arrays
    :param q: no. of equilibrium steps
    :return: two sublattices and values for...
    """
    wb = xlwt.Workbook()  # Setting up spreadsheet to write data too
    ws = wb.add_sheet("LiMn2O4", cell_overwrite_ok=True)
    ws.write(0, 0, "Iteration")
    ws.write(0, 1, "Lattice 1 Occupancy")
    ws.write(0, 2, "Energy 1")
    ws.write(0, 3, "Lattice 2 Occupancy")
    ws.write(0, 4, "Energy 2")
    row = 2

    mu = -4.

    sublattice1 = np.zeros((x, x, x), dtype=np.int)  # First empty sublattice

    sublattice2 = np.zeros((x, x, x), dtype=np.int)  # Generates second empty sublattice

    for itt in range(0, n):
        a = random.randint(0, (x-1))  # Generates random numbers for selection of random sites
        b = random.randint(0, (x-1))
        c = random.randint(0, (x-1))
        d = random.randint(1, 2)
        if d == 1:
            site = sublattice1[a, b, c]  # Selects random spin site
            deltaH = Hamiltonian(sublattice1, sublattice2, x, a, b, c, mu)

            if deltaH <= 0:
                if sublattice1[a, b, c] == 1:  # Change is accepted due to favourable energy change
                    sublattice1[a, b, c] = 0
                else:
                    sublattice1[a, b, c] = 1

            else:
                d = random.random()  # Random number generated for comparison between 0 and 1
                p = np.exp(-deltaH/298.*1.38E-23)  # Probability of spin changing

                if d > p:  # Comparison of random number and probability
                    sublattice1[a, b, c] = site  # State change rejected as probability too low

                else:
                    if sublattice1[a, b, c] == 1:  # Change accepted as probability is high
                        sublattice1[a, b, c] = 0
                    else:
                        sublattice1[a, b, c] = 1
        else:
            site = sublattice2[a, b, c]
            deltaH = Hamiltonian(sublattice2, sublattice1, x, a, b, c, mu)

            if deltaH <= 0:
                if sublattice2[a, b, c] == 1:  # Change is accepted due to favourable energy change
                    sublattice2[a, b, c] = 0
                else:
                    sublattice2[a, b, c] = 1

            else:
                d = random.random()  # Random number generated for comparison between 0 and 1
                p = np.exp(-deltaH/298.*1.38E-23)  # Probability of spin changing

                if d > p:  # Comparison of random number and probability
                    sublattice2[a, b, c] = site  # State change rejected as probability too low

                else:
                    if sublattice2[a, b, c] == 1:  # Change accepted as probability is high
                        sublattice2[a, b, c] = 0
                    else:
                        sublattice2[a, b, c] = 1

        if itt % 1000 == 0:
            nocc1 = occupancy(sublattice1, x)
            occ1 = nocc1/float(x*x*x)
            ws.write(row, 1, occ1)
            nocc2 = occupancy(sublattice2, x)
            occ2 = nocc2/float(x*x*x)
            ws.write(row, 3, occ2)

            u1 = Ulattice(sublattice1, sublattice2, x)
            ws.write(row, 2, u1)
            u2 = Ulattice(sublattice2, sublattice1, x)
            ws.write(row, 4, u2)

            ws.write(row, 0, itt)
            row += 1
            print itt

    print "sublattice1"
    print sublattice1
    print "sublattice2"
    print sublattice2

    wb.save('LiMn2O4equil.xls')

if __name__ == '__main__':
    LiMn2O4(n, x)
