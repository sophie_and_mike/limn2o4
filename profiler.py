import cProfile,pstats
import os

'''
  Runs a test that profiles how long the code spends running. "size" is the size
  of your lattice, "no_iterations" is the number of Monte Carlo runs.
  You can change the argument in "runctx" to profile any function. However, the
  function will need to run for at least a few seconds to give you accurate results
  (so a few thousand or more runs of you MC algorithm may be most suitable).
  I recommend you create a separate directory (in my case called "analysis") and save
  the results in there. You can also take out the line beginning "stream", and the second
  argument "stream=stream" in the line beneath, to output the results to the screen.
  The bottom line cleans up the stats so that they're human readable and sorts them
  according to the cumulative time spent running a particular function.
  
  I recommend inputting the function arguments as command line arguments.
'''

def run_test(size,no_iterations):
  run_number=1
  results_directory='analysis' # You need to have created this directory beforehand.
  filename='/stats%d_%d_%d' % (run_number,size,no_iterations)
  function_name='multiple_monte_carlo(size,no_iterations)' # Change this to the name of your function.
    
  while os.path.isfile(results_directory+filename):
  # Each run is given a unique filename. You'll have to keep a separate record of what all
  # filenames correspond to if you change your code.
    run_number+=1
    filename='/stats%d_%d_%d' % (run_number,size,no_iterations)
    
  cProfile.runctx(function_name,globals(),locals(),'.prof')  
  stream=open(results_directory+filename,'w')
  s = pstats.Stats('.prof',stream=stream)
  s.strip_dirs().sort_stats('cumulative').print_stats(30)
